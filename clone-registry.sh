# Clone the repository
echo "Get glottolog registry from https://github.com/glottolog/glottolog.git"
git clone --progress --verbose https://github.com/glottolog/glottolog.git /glottolog-registry
# Checkout the latest tag
cd /glottolog-registry
git checkout tags/$(git describe --tags $(git rev-list --tags --max-count=1)) && echo Current registry tag now set to $(git describe --tags --abbrev=0)
