from fastapi import FastAPI, HTTPException
from pycountry import languages
from pyglottolog import Glottolog

try:
    from glottolog.config import GLOTTOLOG_REGISTRY
except ModuleNotFoundError:
    from config import GLOTTOLOG_REGISTRY

GLOTTOLOG = Glottolog(GLOTTOLOG_REGISTRY, cache=True)
app = FastAPI()


@app.get("/")
def root():
    return {"message": "Glottolog REST API Service"}


@app.get("/check/{id}")
def check_id(id: str):
    languoid = GLOTTOLOG.languoid(id)
    if not languoid:
        return False
    return True


@app.get("/name/{id}")
def get_name(id: str):
    languoid = GLOTTOLOG.languoid(id)
    if not languoid:
        raise HTTPException(status_code=404, detail={"message": "glottocode invalid or not found", "code": 404})
    result = {"name": languoid.name}
    return result


@app.get("/alternatives/{id}")
def get_alternatives(id: str):
    languoid = GLOTTOLOG.languoid(id)
    if not languoid:
        raise HTTPException(status_code=404, detail={"message": "glottocode invalid or not found", "code": 404})
    languoid_alternatives = languoid.names
    result = []
    if languoid_alternatives:
        alternatives = list()
        # keep only english lexvo and all wals and multitree
        if languoid_alternatives.get('wals'):
            alternatives.extend(languoid_alternatives.get('wals'))
        # if languoid_alternatives.get('multitree'):
        #     alternatives.extend(languoid_alternatives.get('multitree'))
        if languoid_alternatives.get('lexvo'):
            for alt_lexvo in languoid_alternatives.get('lexvo'):
                if alt_lexvo.endswith('[en]'):
                    alternatives.append(alt_lexvo.split('[')[0].strip())
        # cleanup duplicates that match the base name
        for alt in alternatives:
            if alt == languoid.name:
                alternatives.remove(alt)
        result = {"name": languoid.name, "alternatives": alternatives}
    return result


@app.get("/glottocode/{iso}")
def get_glottocode(iso: str):
    try:
        glottocode = GLOTTOLOG.languoid(languages.get(alpha_2=iso).alpha_3).glottocode
    except AttributeError:
        try:
            glottocode = GLOTTOLOG.languoid(languages.get(alpha_3=iso).alpha_3).glottocode
        except AttributeError:
            glottocode = None
    return {'glottocode': glottocode}
