import sys
import smtplib
import traceback
from email.message import EmailMessage

from config import EMAIL_HOST, EMAIL_PORT, EMAIL_HOST_USER, EMAIL_HOST_PASSWORD, EMAIL_RECIPIENTS

msg = EmailMessage()
msg.set_content(f'The version of Glottolog registry has been updated to the latest version {sys.argv[1]}\n'
                f'https://github.com/glottolog/glottolog')

msg['Subject'] = f'Glottolog registry update'
msg['From'] = EMAIL_HOST_USER
msg['To'] = EMAIL_RECIPIENTS
try:
    server = smtplib.SMTP(EMAIL_HOST, 25)
    server.connect(EMAIL_HOST, EMAIL_PORT)
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(EMAIL_HOST_USER, EMAIL_HOST_PASSWORD)
    server.send_message(msg)
    server.quit()
except:
    traceback.print_exc()
    print('Something went wrong...')
