import os
GLOTTOLOG_REGISTRY = os.environ.get('registry')

EMAIL_HOST = os.environ.get('email_host')
EMAIL_HOST_USER = os.environ.get('email_user')
EMAIL_HOST_PASSWORD = os.environ.get('email_pass')
EMAIL_PORT = int(os.environ.get('email_port'))
EMAIL_RECIPIENTS = os.environ.get('recipients')
