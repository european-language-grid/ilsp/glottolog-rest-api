OSNAME=$(awk -F'=' '/^NAME/ {print $2}' /etc/os-release | tr -d '"')

if [ "$OSNAME" = "Alpine Linux" ]; then
  echo $OSNAME
  echo "Starting cron daemon..."
  crond
fi
echo "Starting Gunicorn workers..."
gunicorn -w 4 -k uvicorn.workers.UvicornWorker main:app --timeout=180 --bind=0.0.0.0:8001 \
--error-logfile log/api.log: --log-file log/api.log \
--reload --capture-output --enable-stdio-inheritance