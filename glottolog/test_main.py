from fastapi.testclient import TestClient

from glottolog.main import app

client = TestClient(app)


def test_root():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {'message': 'Glottolog REST API Service'}


def test_get_name():
    response = client.get("/name/bava1246")
    assert response.status_code == 200
    assert response.json() == {'name': 'Bavarian'}


def test_get_alternatives():
    response = client.get("/alternatives/bava1246")
    assert response.status_code == 200
    expected = {
        "name": "Bavarian",
        "alternatives": [
            "German (Bavarian)",
            "German (Upper Austrian)",
            "German (Viennese)",
            "Bairisch",
            "Bavarian Austrian",
            "Bavarois",
            "Bayerisch",
            "Bávaro",
            "German (Bavarian)",
            "Ost-Oberdeutsch",
            "Austro-Bavarian language"
        ]
    }
    assert response.json() == expected


def test_check_id():
    response = client.get("/check/bava1246")
    assert response.status_code == 200
    assert response.json() is True


def test_get_invalid_name():
    response = client.get("/name/foo")
    assert response.status_code == 404
    assert response.json() == {"detail": {"message": "glottocode invalid or not found", "code": 404}}


def test_get_invalid_alternatives():
    response = client.get("/alternatives/foo")
    assert response.status_code == 404
    assert response.json() == {"detail": {"message": "glottocode invalid or not found", "code": 404}}


def test_check_invalid_id():
    response = client.get("/check/foo")
    assert response.status_code == 200
    assert response.json() is False
