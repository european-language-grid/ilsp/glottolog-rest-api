FROM python:3.8.1-alpine3.11

RUN apk update \
    && apk add --virtual build-deps build-base gcc musl-dev \
    && apk add --no-cache bash git openrc

ADD ./glottolog/requirements.txt /requirements.txt
RUN pip install -r requirements.txt

RUN mkdir -p /glottolog-registry
WORKDIR /glottolog-registry
ADD ./registry .

WORKDIR /glottolog_service
ADD ./glottolog ./glottolog
ADD clone-registry.sh ./clone-registry.sh
ADD update-registry.sh ./update-registry.sh

COPY update-registry-cron /etc/cron.d/update-registry-cron
RUN chmod +x clone-registry.sh \
    && chmod +x glottolog/deploy.sh \
    && chmod +x update-registry.sh \
    && chmod 0644 /etc/cron.d/update-registry-cron \
    && crontab /etc/cron.d/update-registry-cron \
    && touch /var/log/cron.log
WORKDIR /glottolog_service/glottolog
ENTRYPOINT ./deploy.sh