cd /glottolog-registry
CURRENT_TAG=$(git describe --tags --abbrev=0)
LATEST_TAG=$(git describe --tags $(git rev-list --tags --max-count=1))
echo `date +%d/%m/%Y\ %H:%M:%S`
echo "Current registry tag: $CURRENT_TAG"
if [ "$CURRENT_TAG" = "$LATEST_TAG" ]; then
    echo "CURRENT TAG $CURRENT_TAG is the latest."
else
    echo "NEW TAG $LATEST_TAG is found. Checking out tag $LATEST_TAG"
    git checkout tags/$(git describe --tags $(git rev-list --tags --max-count=1)) && echo Current registry tag now set to $(git describe --tags --abbrev=0) \
    && echo "Sending email to admins" && python /glottolog_service/send_email.py $CURRENT_TAG $LATEST_TAG

fi
echo $'\n'